require 'blather/client'

client = setup 'apollo1cp2@gmail.com', 'cp2-2013'


when_ready do
	client.roster.grouped.each do |group, items|
		puts "#{'*'*3} #{group || 'Ungrouped'} #{'*'*3}"
		items.each { |item| puts "- #{item.name} (#{item.jid})" }
		puts
	end
end

client.register_handler :subscription, :request? do |s|
  client.write s.approve!
end

client.register_handler :message, :chat?, :body => 'exit' do |m|
  client.write Blather::Stanza::Message.new(m.from, 'Exiting...')
  client.close
end

client.register_handler :message, :chat?, :body do |m|
  puts "#{m.from}: #{m.body}"
end
